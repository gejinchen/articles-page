'use strict'
var article_0 = {
        title  : "Life in UOB",
        author : "Trump",
        date   : " 2017-3-4",
        img    : '../img/UOB_sm.jpg',
        text: "The University of Bristol (abbreviated as Bris. in post-nominal letters, sometimes referred to as Bristol University) is a red brick research university located in Bristol, United Kingdom.[8] It received its royal charter in 1909,[9] and its predecessor institution, University College, Bristol, had been in existence since 1876.[10]Bristol is organised into six academic faculties composed of multiple schools and departments running over 200 undergraduate courses largely situated in the Tyndalls Park area of the city.[11] The university had a total income of £565.8 million in 2015/16, of which £146.2 million was from research grants and contracts.[2] It is the largest independent employer in Bristol.[12]The University of Bristol is ranked 11th in the UK for its research, according to the Research Excellence Framework (REF) 2014 by GPA.[13] The University of Bristol is ranked 37th by the QS World University Rankings 2015-16, and is ranked amongst the top of UK universities by QS,[14] THE,[15] and ARWU. A highly selective institution, it has an average of 6.4 (Sciences faculty) to 13.1 (Medicine & Dentistry Faculty) applicants for each undergraduate place.[16] The University of Bristol is the youngest British university to be ranked among the top 40 institutions in the world according to the QS World University Rankings, and has also been ranked at 15th in the world in terms of reputation with employers, placing higher than several American Ivy League universities, including Princeton University, Cornell and UPenn.[17]Current academics include 21 fellows of the Academy of Medical Sciences, 13 fellows of the British Academy, 13 fellows of the Royal Academy of Engineering and 44 fellows of the Royal Society.[18] The university has been associated with 13 Nobel laureates throughout its history, including Paul Dirac, Sir William Ramsay, Cecil Frank Powell, Sir Winston Churchill, Dorothy Hodgkin, Hans Albrecht Bethe, Max Delbrück, Gerhard Herzberg, Sir Nevill Francis Mott, Sir Paul Nurse, Harold Pinter, Jean-Marie Gustave Le Clézio and most recently, 2015 Economics Nobel Prize winner Angus Deaton.Bristol is a member of the Russell Group of research-intensive British universities,[19] the European-wide Coimbra Group[20] and the Worldwide Universities Network, of which the university's previous vice-chancellor, Eric Thomas, was chairman from 2005 to 2007.[21] In addition, the university holds an Erasmus Charter, sending more than 500 students per year to partner institutions in Europe.[22]"
    };

var article_1 = {
        title  : "Life in UOB",
        author : "Trump",
        date   : " 2017-3-4",
        img    : '../img/Paris_sm.jpg',
        text: "The University of Bristol (abbreviated as Bris. in post-nominal letters, sometimes referred to as Bristol University) is a red brick research university located in Bristol, United Kingdom.[8] It received its royal charter in 1909,[9] and its predecessor institution, University College, Bristol, had been in existence since 1876.[10]Bristol is organised into six academic faculties composed of multiple schools and departments running over 200 undergraduate courses largely situated in the Tyndalls Park area of the city.[11] The university had a total income of £565.8 million in 2015/16, of which £146.2 million was from research grants and contracts.[2] It is the largest independent employer in Bristol.[12]The University of Bristol is ranked 11th in the UK for its research, according to the Research Excellence Framework (REF) 2014 by GPA.[13] The University of Bristol is ranked 37th by the QS World University Rankings 2015-16, and is ranked amongst the top of UK universities by QS,[14] THE,[15] and ARWU. A highly selective institution, it has an average of 6.4 (Sciences faculty) to 13.1 (Medicine & Dentistry Faculty) applicants for each undergraduate place.[16] The University of Bristol is the youngest British university to be ranked among the top 40 institutions in the world according to the QS World University Rankings, and has also been ranked at 15th in the world in terms of reputation with employers, placing higher than several American Ivy League universities, including Princeton University, Cornell and UPenn.[17]Current academics include 21 fellows of the Academy of Medical Sciences, 13 fellows of the British Academy, 13 fellows of the Royal Academy of Engineering and 44 fellows of the Royal Society.[18] The university has been associated with 13 Nobel laureates throughout its history, including Paul Dirac, Sir William Ramsay, Cecil Frank Powell, Sir Winston Churchill, Dorothy Hodgkin, Hans Albrecht Bethe, Max Delbrück, Gerhard Herzberg, Sir Nevill Francis Mott, Sir Paul Nurse, Harold Pinter, Jean-Marie Gustave Le Clézio and most recently, 2015 Economics Nobel Prize winner Angus Deaton.Bristol is a member of the Russell Group of research-intensive British universities,[19] the European-wide Coimbra Group[20] and the Worldwide Universities Network, of which the university's previous vice-chancellor, Eric Thomas, was chairman from 2005 to 2007.[21] In addition, the university holds an Erasmus Charter, sending more than 500 students per year to partner institutions in Europe.[22]"
    };

var article_2 = {
        title  : "Life in UOB",
        author : "Trump",
        date   : " 2017-3-4",
        img    : '../img/Berlin_sm.jpg',
        text: "The University of Bristol (abbreviated as Bris. in post-nominal letters, sometimes referred to as Bristol University) is a red brick research university located in Bristol, United Kingdom.[8] It received its royal charter in 1909,[9] and its predecessor institution, University College, Bristol, had been in existence since 1876.[10]Bristol is organised into six academic faculties composed of multiple schools and departments running over 200 undergraduate courses largely situated in the Tyndalls Park area of the city.[11] The university had a total income of £565.8 million in 2015/16, of which £146.2 million was from research grants and contracts.[2] It is the largest independent employer in Bristol.[12]The University of Bristol is ranked 11th in the UK for its research, according to the Research Excellence Framework (REF) 2014 by GPA.[13] The University of Bristol is ranked 37th by the QS World University Rankings 2015-16, and is ranked amongst the top of UK universities by QS,[14] THE,[15] and ARWU. A highly selective institution, it has an average of 6.4 (Sciences faculty) to 13.1 (Medicine & Dentistry Faculty) applicants for each undergraduate place.[16] The University of Bristol is the youngest British university to be ranked among the top 40 institutions in the world according to the QS World University Rankings, and has also been ranked at 15th in the world in terms of reputation with employers, placing higher than several American Ivy League universities, including Princeton University, Cornell and UPenn.[17]Current academics include 21 fellows of the Academy of Medical Sciences, 13 fellows of the British Academy, 13 fellows of the Royal Academy of Engineering and 44 fellows of the Royal Society.[18] The university has been associated with 13 Nobel laureates throughout its history, including Paul Dirac, Sir William Ramsay, Cecil Frank Powell, Sir Winston Churchill, Dorothy Hodgkin, Hans Albrecht Bethe, Max Delbrück, Gerhard Herzberg, Sir Nevill Francis Mott, Sir Paul Nurse, Harold Pinter, Jean-Marie Gustave Le Clézio and most recently, 2015 Economics Nobel Prize winner Angus Deaton.Bristol is a member of the Russell Group of research-intensive British universities,[19] the European-wide Coimbra Group[20] and the Worldwide Universities Network, of which the university's previous vice-chancellor, Eric Thomas, was chairman from 2005 to 2007.[21] In addition, the university holds an Erasmus Charter, sending more than 500 students per year to partner institutions in Europe.[22]"
    };

var article_3 = {
        title  : "Life in UOB",
        author : "Trump",
        date   : " 2017-3-4",
        img    : '../img/rio_sm.jpg',
        text: "The University of Bristol (abbreviated as Bris. in post-nominal letters, sometimes referred to as Bristol University) is a red brick research university located in Bristol, United Kingdom.[8] It received its royal charter in 1909,[9] and its predecessor institution, University College, Bristol, had been in existence since 1876.[10]Bristol is organised into six academic faculties composed of multiple schools and departments running over 200 undergraduate courses largely situated in the Tyndalls Park area of the city.[11] The university had a total income of £565.8 million in 2015/16, of which £146.2 million was from research grants and contracts.[2] It is the largest independent employer in Bristol.[12]The University of Bristol is ranked 11th in the UK for its research, according to the Research Excellence Framework (REF) 2014 by GPA.[13] The University of Bristol is ranked 37th by the QS World University Rankings 2015-16, and is ranked amongst the top of UK universities by QS,[14] THE,[15] and ARWU. A highly selective institution, it has an average of 6.4 (Sciences faculty) to 13.1 (Medicine & Dentistry Faculty) applicants for each undergraduate place.[16] The University of Bristol is the youngest British university to be ranked among the top 40 institutions in the world according to the QS World University Rankings, and has also been ranked at 15th in the world in terms of reputation with employers, placing higher than several American Ivy League universities, including Princeton University, Cornell and UPenn.[17]Current academics include 21 fellows of the Academy of Medical Sciences, 13 fellows of the British Academy, 13 fellows of the Royal Academy of Engineering and 44 fellows of the Royal Society.[18] The university has been associated with 13 Nobel laureates throughout its history, including Paul Dirac, Sir William Ramsay, Cecil Frank Powell, Sir Winston Churchill, Dorothy Hodgkin, Hans Albrecht Bethe, Max Delbrück, Gerhard Herzberg, Sir Nevill Francis Mott, Sir Paul Nurse, Harold Pinter, Jean-Marie Gustave Le Clézio and most recently, 2015 Economics Nobel Prize winner Angus Deaton.Bristol is a member of the Russell Group of research-intensive British universities,[19] the European-wide Coimbra Group[20] and the Worldwide Universities Network, of which the university's previous vice-chancellor, Eric Thomas, was chairman from 2005 to 2007.[21] In addition, the university holds an Erasmus Charter, sending more than 500 students per year to partner institutions in Europe.[22]"
    };


var articles = [article_0, article_1, article_2, article_3];
$(document).ready(function() {
    // place for getting articles from the backend
    addArticle();
    $("#news-slider").owlCarousel({
        items:3,
        itemsDesktop:[1199,2],
        itemsDesktopSmall:[980,2],
        itemsMobile:[600,1],
        pagination:false,
        navigationText:false,
        autoPlay:true
    });
    $("#news-slider1").owlCarousel({
        items:3,
        itemsDesktop:[1199,2],
        itemsDesktopSmall:[980,2],
        itemsMobile:[600,1],
        pagination:false,
        navigationText:false,
        autoPlay:true
    });
    setModals();
});

function makeArticle(index) {
    var content = "";
    content +=  "<div class='post-slide modals'>"+
                    "<div class='post-img'>"+
                        "<img src='" + articles[index].img + "' alt='" + articles[index].imgName + "'>" +
                    "</div>"+
                    "<div class='post-content'>"+
                        "<h3 class='post-title'><a href='#'>"+articles[index].title+"</a></h3>"+
                        "<p style='height: 96px;' class='post-description'>"+
                            ""+articles[index].text+""+
                        "</p>"+
                        "<ul class='post-bar'>"+
                            "<li><i class='fa fa-calendar'></i>"+articles[index].date+"</li>"+
                            "<li>"+
                                "<i class='fa fa-folder'></i>"+
                                "<a href='#'>"+articles[index].author+"</a>"+
                            "</li>"+
                        "</ul>"+
                        "<a class='read-more' data-toggle='modal' data-target='#myModal' style='cursor:pointer;'>read more</a>"+
                    "</div>"+
                "</div>";
    return content;
}


function addArticle() {
    for(var i = 0; i < articles.length; i++) {
        $("#news-slider").append(makeArticle(i));
    }
    for(var i = 0; i < articles.length; i++) {
        $("#news-slider1").append(makeArticle(i));
    }
}

function setModals() {
    var divs = document.getElementsByClassName("modals");
    for(var i = 0; i < divs.length; i++) {
        var button = divs[i].getElementsByClassName("read-more");
        button[0].onclick=function(){showModal(this.parentNode.parentNode, i)};
    }
}

function showModal(thisdiv, index) {
    $('#modal-content-holder').empty();
    $('#modal-header-holder').empty();
    var imgs = thisdiv.getElementsByTagName("img");
    var path = imgs[0].src;
    var arts = thisdiv.getElementsByClassName("post-title");
    var path0 = arts[0].innerText;
    var texts = thisdiv.getElementsByClassName("post-description");
    var text = texts[0].innerText;


    console.log(thisdiv);
    $("#modal-content-holder").append("<img src='"+path+"' width='40%' style='margin: 0 auto;display:block;' alt=''> \
                                        <P style='color:black; margin-top:10px;'>"+text+"<p>");
    $("#modal-header-holder").append("<button class='close' data-dismiss='modal'><span>&times;</span></button>\
                                     <h4 style='margin: 0;color:#666;'>"+path0+"</h4>");
}


$(function () {
  $('.carousel-control').css('line-height', $('.carousel-inner img').height() + 'px');
  $(window).resize(function () {
    var $height = $('.carousel-inner img').eq(0).height() ||
                  $('.carousel-inner img').eq(1).height() ||
                  $('.carousel-inner img').eq(2).height();
    $('.carousel-control').css('line-height', $height + 'px');
  });
});


$('#myCarousel').carousel({
  interval : 4000,
});
